#ifndef WAVEWINDOW_H
#define WAVEWINDOW_H

#include <QMainWindow>

#include "wave.h"


namespace Ui {
class WaveWindow;
}

class WaveWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit WaveWindow(QWidget *parent = 0);
    ~WaveWindow();

    void init_wave(void);

    void show_wave(wave_info_struct* wave_info);

private:
    Ui::WaveWindow *ui;

    wave_info_struct wave1;
    //WaveWindow *my_wave_window;

};

#endif // WAVEWINDOW_H
