#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <Qdebug>

namespace Ui {
class MainWindow;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void init_serial();
private slots:
    void on_openButton_clicked();

    void on_flushButton_clicked();

    void on_sendButton_clicked();

    void read_data();

    void on_stopserialButton_clicked();

    void on_pushButton_clicked();

    void parse_data(QByteArray array_data);//数据解析

    void on_receiverHex_toggled(bool checked);

    void display_data_on_lenedit(int type, int position, QByteArray data);//显示数据

    void on_showwaveButton_clicked();

private:
    Ui::MainWindow *ui;
    QSerialPort *serial;
    QByteArray string_array;
    QByteArray append_str_array;
    int hex_show;
};

enum DATA_TYPE
{
    INT_TYPE,
    FLOAT_TYPE = 10
};

enum DATA_POSITION
{
    LINE_EDITE0,
    LINE_EDITE1,
    LINE_EDITE2,
    LINE_EDITE3,
    LINE_EDITE4,
    LINE_EDITE5,
    LINE_EDITE6,
    LINE_EDITE7,
    LINE_EDITE8,
    LINE_EDITE9,
    LINE_EDITE10,
    LINE_EDITE11,

};


#endif // MAINWINDOW_H
