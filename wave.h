#ifndef WAVE_H
#define WAVE_H


#include "qcustomplot.h"


typedef struct wave_info_struct_t
{
    QCustomPlot * customplot;//折线图位置
    QString title;//折线图名字
    QString x_name;//x轴名字
    QString y_name;//Y轴名字
    unsigned int line_num;//线条数量
}wave_info_struct;

#endif // WAVE_H
