#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "data_processing.h"


//#include "qcustomplot.h"
#include "wavewindow.h"

#define BUFF_SIZE 64

/*全局变量*/
float float_data = 0.0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //serial = new QSerialPort;

    //初始化串口
    init_serial();

}


MainWindow::~MainWindow()
{
    delete ui;
}
/********************************************
@function name:
@function detail:串口初始化
*******************************************/
void MainWindow::init_serial()
{
    //查找可用的串口
    QSerialPort serial;
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        serial.setPort(info);
        if(serial.open(QIODevice::ReadWrite))
        {
            ui->serialBox->addItem(serial.portName());
            serial.close();
        }
    }

    ui->baudrateBox->addItem("9600");
    ui->baudrateBox->addItem("1200");
    ui->baudrateBox->addItem("2400");
    ui->baudrateBox->addItem("4800");
    ui->baudrateBox->addItem("14400");
    ui->baudrateBox->addItem("19200");
    ui->baudrateBox->addItem("38400");
    ui->baudrateBox->addItem("56000");
    ui->baudrateBox->addItem("115200");
    ui->baudrateBox->addItem("128000");
    ui->baudrateBox->addItem("256000");

    ui->databitBox->addItem("8");
    ui->databitBox->addItem("5");
    ui->databitBox->addItem("6");
    ui->databitBox->addItem("7");

    ui->paritycheckBox->addItem("none");
    ui->paritycheckBox->addItem("odd");//奇校验
    ui->paritycheckBox->addItem("even");//偶校验

    ui->stopbitBox->addItem("1");
    ui->stopbitBox->addItem("1.5");
    ui->stopbitBox->addItem("2");

    ui->flowBox->addItem("off");
    ui->flowBox->addItem("hardware");
    ui->flowBox->addItem("xonxoff");

    //设置波特率下拉菜单默认显示第三项
    ui->baudrateBox->setCurrentIndex(8);
    //关闭发送按钮的使能
    //ui->sendButton->setEnabled(false);
    qDebug() << tr("初始化串口设定成功！");
}

/********************************************
@function name:
@function detail:打开串口按按键按下
*******************************************/
void MainWindow::on_openButton_clicked()
{
    if(ui->openButton->text()==tr("打开串口"))
    {
        serial = new QSerialPort;//打开串口按键按下一次，new 一个新的对象，以便更改串口
        //设置串口名
        serial->setPortName(ui->serialBox->currentText());
        //打开串口
        serial->open(QIODevice::ReadWrite);
        //设置波特率
        serial->setBaudRate(115200);//(ui->baudrateBox->currentText().toInt());
        //设置数据位数
        switch(ui->databitBox->currentIndex())
        {
        case 8: serial->setDataBits(QSerialPort::Data8); break;
        default: break;
        }
        //设置奇偶校验
        switch(ui->baudrateBox->currentIndex())
        {
        case 0: serial->setParity(QSerialPort::NoParity); break;
        default: break;
        }
        //设置停止位
        switch(ui->stopbitBox->currentIndex())
        {
        case 1: serial->setStopBits(QSerialPort::OneStop); break;
        case 2: serial->setStopBits(QSerialPort::TwoStop); break;
        default: break;
        }
        //设置流控制
        serial->setFlowControl(QSerialPort::NoFlowControl);
        //关闭设置菜单使能
        ui->serialBox->setEnabled(false);
        ui->baudrateBox->setEnabled(false);
        ui->databitBox->setEnabled(false);
        ui->paritycheckBox->setEnabled(false);
        ui->stopbitBox->setEnabled(false);
        ui->openButton->setText(tr("关闭串口"));
        ui->sendButton->setEnabled(true);
        //连接信号槽
        QObject::connect(serial, &QSerialPort::readyRead, this, &MainWindow::read_data);
    }
    else
    {
        //关闭串口
        serial->clear();
        serial->close();
        serial->deleteLater();
        //恢复设置使能
        ui->serialBox->setEnabled(true);
        ui->baudrateBox->setEnabled(true);
        ui->databitBox->setEnabled(true);
        ui->paritycheckBox->setEnabled(true);
        ui->stopbitBox->setEnabled(true);
        ui->openButton->setText(tr("打开串口"));
        ui->sendButton->setEnabled(false);
    }
}


/********************************************
@function name:
@function detail:刷新串口按键按下
*******************************************/
void MainWindow::on_flushButton_clicked()
{

}
/********************************************
@function name:
@function detail:发送数据按键按下
*******************************************/
void MainWindow::on_sendButton_clicked()
{
    serial->write(ui->textEdit->toPlainText().toLatin1());
}
/********************************************
@function name:
@function detail:读取串口数据回调函数,
*******************************************/
void MainWindow::read_data()
{
    if(string_array.length() > BUFF_SIZE)
    {
        string_array.clear();
    }
    string_array = serial->readAll();
    qDebug()<<"原始数据:"<<string_array<<endl;
    qDebug()<<"原始数据长度:"<<string_array.length()<<endl;
    //qDebug()<<"原始数据10进制:"<<string_array.mid(8,8).toFloat(&ok)<<endl;
    if(!string_array.isEmpty())
    {
        QString str = ui->textBrowser->toPlainText();
        str+=tr(string_array);
        ui->textBrowser->clear();
        ui->textBrowser->append(str);

        append_str_array = string_array;
        //append_str_array.append(string_array);//会导致数据累加
        if(append_str_array.length() != 0)
        {
            //解析数据
            parse_data(append_str_array);
        }
    }
    string_array.clear();

}
/********************************************
@function name:
@function detail:解码函数
AA 55 10 00 00 00 A0 F0 F1 E1 08 22 30 24 C4 0C AA 55 10 00 00 00 A0 F0 F1 E1 08 22 30 24 C4 0C
*******************************************/
void MainWindow::parse_data(QByteArray array_data)
{
    int index = 0;
    int len = 0;
    int data_position = 0;

    QByteArray data_type;
    QByteArray byte_array = array_data.toHex();
    len = byte_array.length();
    if(len > BUFF_SIZE)
    {
        //byte_array.clear();
    }
    qDebug()<<"转换16进制数据"<<byte_array<<"长度"<<len<<endl;
    //16进制界面上显示
    if(hex_show == 1)
    {
        ui->textBrowser->clear();
        ui->textBrowser->append(byte_array);
    }

    index = byte_array.indexOf("aa55");//定位帧头,这里的帧头本来是aa55的 传输中为什么有错误我也不知道
    if(index != -1)
    {
        data_type = byte_array.mid(index + 4, 2);//查看类型
        data_position = byte_array.mid(index + 6, 2).toInt(0,16);//linEdite位置
        if(data_type == "00")//int 类型
        {
            qDebug()<<"收到int 类型"<<endl;
            display_data_on_lenedit(0, data_position, byte_array.mid(index + 8, 8));
        }
        if(data_type == "10")//float类型
        {
            qDebug()<<"收到float 类型"<<endl;
            display_data_on_lenedit(10, data_position, byte_array.mid(index + 8, 8));
        }
    }
    //防止数据太多
    if(len > 32)
        byte_array.clear();
}

/********************************************
@function name:
@function detail:停止读取串口数据函数,
*******************************************/
void MainWindow::on_stopserialButton_clicked()
{

}


/********************************************
@function name:
@function detail:16进制显示串口数据回调函数,
*******************************************/
void MainWindow::on_receiverHex_toggled(bool checked)
{
    if(checked)
    {
        hex_show = 1;
    }
    else
    {
        hex_show = 0;
    }
}

/********************************************
@function name:
@function detail:清除窗口显示,暂时还没啥效果
*******************************************/
void MainWindow::on_pushButton_clicked()
{
    string_array.clear();
    ui->textBrowser->clear();
    //ui->data1->setText();
}

#if 0
void MianWindow::jiema()
{
    QByteArray asciiText = ui->receiverArea->toPlainText().replace(QRegExp("\\[\\d\\d:\\d\\d:\\d\\d\\.\\d\\d\\d\\]\\s"), "").toUtf8();
    QString hexText = QString::fromUtf8(asciiText.toHex().toUpper());
}
#endif

/********************************************
@function name:
@function detail:将数据显示到界面的lineEdit 里面
*******************************************/
void MainWindow::display_data_on_lenedit(int type, int position, QByteArray data)
{
    int int_data = 0;
    int temp = 0;
   // float float_data = 0.0;
    bool ok;
    if(type == INT_TYPE)
    {
        int_data = data.toInt(&ok, 16);

        switch(position)
        {
            case LINE_EDITE0 :
                ui->data1->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE1 :
                ui->data1_2->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE2 :
                ui->data1_3->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE3 :
                ui->data1_4->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE4 :
                ui->data1_5->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE5 :
                ui->data1_6->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE6 :
                ui->data1_7->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE7 :
                ui->data1_8->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE8 :
                ui->data1_9->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE9 :
                ui->data1_10->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE10 :
                ui->data1_11->setText(QString::number(int_data));//小数点后6位
                break;
            case LINE_EDITE11 :
                ui->data1_12->setText(QString::number(int_data));//小数点后6位
                break;
            default: break;

        }
    }
    if(type == FLOAT_TYPE)
    {
        temp = data.toInt(0,16);
        float_data = *(float*)&temp;

        switch(position)
        {
            case LINE_EDITE0 :
                ui->data1->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE1 :
                ui->data1_2->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE2 :
                ui->data1_3->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE3 :
                ui->data1_4->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE4 :
                ui->data1_5->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE5 :
                ui->data1_6->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE6 :
                ui->data1_7->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE7 :
                ui->data1_8->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE8 :
                ui->data1_9->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE9 :
                ui->data1_10->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE10 :
                ui->data1_11->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            case LINE_EDITE11 :
                ui->data1_12->setText(QString::number(float_data,'f',6));//小数点后6位
                break;
            default: break;

        }
    }
}
/********************************************
@function name:
@function detail:按下波形显示按钮
*******************************************/
void MainWindow::on_showwaveButton_clicked()
{
    this->close();
    WaveWindow* wave_window = new WaveWindow;

    wave_window->show();
}
